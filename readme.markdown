demo of using tup with python
=============================

### what's tup?

[tup!](http://gittup.org/tup/)


### running a build & test from scratch with tup

    $ tup


### running an incremental build & test with tup immediately after doing tup

    $ tup

produces the following output

    $ tup
    [ tup ] [0.001s] Scanning filesystem...
    [ tup ] [0.004s] Reading in new environment variables...
    [ tup ] [0.005s] No Tupfiles to parse.
    [ tup ] [0.006s] No files to delete.
    [ tup ] [0.006s] No commands to execute.
    [ tup ] [0.006s] Updated.


Note how when it was run the second time,

1.  tup didnt do anything; in fact
2.  it didnt do anything *really quickly*


### visualising tup dependencies:

do something like this:

    tup graph --dirs . | dot -Tpng > g.png

to get ![a nice graph of dependencies](https://bitbucket.org/rfc/tup_py_demo/raw/master/vanity/g.png)



tricks required to make this work
---------------------------------


many of the difficulties here are resolving the disagreement between Tup, which regards reading from or writing to a file not specified in the build script as a **build error**, and python / setuptools / py.test, which like to crap temporary / working / cache files all over the file system during normal operation.



1.  run `python` with `-B` argument to avoid making `*.pyc` and `__pycache__`. We avoid doing this as otherwise we have to explicitly declare these files as build outputs in the `Tupfile`
2.  run `python setup.py` from inside throwaway temp directories. Invoking `setup.py` generates a heap of working files in addition to the final products of the build (an installable wheel archive or exe, say). Again, Tup requires us to list these working files as explicit outputs of the build step, unless those files are written to a temporary directory.
3.  run `py.test` as `python -B -mpytest`, again to avoid making `*.pyc` and `__pycache__`
4.  run `py.test` with the optional `--assert=plain` and `--capture=sys` arguments. both of these `py.test` features break when running from inside a Tup build, due to Tup's virtualisation of the filesystem under Windows.
5.  patch & recompile the windows build of `Tup` to use case insensitive string comparison when deciding if a path is contained in the system temporary directory. The latest development version of `Tup` does this with case sensitive comparison, which is incorrect. This can be done by replacing a `strncmp` call with `strncasecmp`. See `patchery`


cross compiling from linux to windows:
--------------------------------------

see https://groups.google.com/d/msg/tup-users/Yj9A6nYF4Dg/14k9Kyzv7qIJ

