from distutils.core import setup
import py2exe

setup(
    console=['foo/script.py'],
    options={
        'py2exe' : {
            'packages' : ['foo'],
            'compressed' : True,
            'bundle_files' : 1,
            'dll_excludes' : ['w9xpopen.exe'],
        }
    },
)
