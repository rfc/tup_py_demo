import foo.a
import foo.b

def test_thing():
    assert foo.a.f(foo.b.g(3)) > 0

def test_unity():
    assert 1 * 1 == 1

def test_zero():
    assert not 0
