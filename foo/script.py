import foo.a

def main():
    import sys
    for arg in sys.argv[1:]:
        print foo.a.f(int(arg))

if __name__ == '__main__':
    main()
