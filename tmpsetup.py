"""
stupid wrapper for python setup.py that invokes it from inside the temp directory

this hides the many files that setup.py likes to write from the wrath of tup.
after setup.py-ing we copy the contents of dist/ into the out_dir/

only these files are shown to tup as outputs, so only these must be declared
in the build script.
"""

import shutil
import tempfile
import argparse
import os
import os.path
import subprocess
import glob
import platform


def main():
    p = argparse.ArgumentParser()
    p.add_argument('--lib', type=str, required=True)
    p.add_argument('--setup', type=str, required=True)
    p.add_argument('--action', type=str, required=True)
    p.add_argument('--out_dir', type=str, required=True)

    args = p.parse_args()

    lib_name = os.path.basename(args.lib)

    d = tempfile.mkdtemp()

    print d

    shutil.copytree(args.lib, os.path.join(d, lib_name))
    shutil.copy(args.setup, os.path.join(d, 'setup.py'))

    print d
    print os.listdir(d)

    cmd = [
        'python',
        '-B', # dont cache pyc files, just in case.
        'setup.py',
        args.action, # e.g. 'bdist_wheel' or 'py2exe' or something
    ]
    p = subprocess.Popen(cmd, cwd=d)
    p.communicate()
    assert p.returncode == 0

    copy_into(os.path.join(d, 'dist'), args.out_dir)


def copy_into(src, dst):
    # shutil.copytree refuses to copy into a dir that already exists.
    # so we hack together a variant that does.

    for (dirpath, dirnames, filenames) in os.walk(src):
        for f in filenames:
            source_f = os.path.join(dirpath, f)
            dest_f = os.path.join(dst, os.path.relpath(dirpath, src), f)
            print "copy %r -> %r" % (source_f, dest_f)
            shutil.copy(source_f, dest_f)


if __name__ == '__main__':
    main()

