import py.test
import subprocess


@py.test.fixture()
def script_path():
    return 'dist_exe/script.exe' # hardcoded badness


def test_script(script_path):

    p = subprocess.Popen([script_path, '1', '2', '3', '4'], stdout=subprocess.PIPE)

    out, err = p.communicate()
    assert out == '2\r\n3\r\n4\r\n5\r\n'
    
